#!/bin/bash

# get all repositories from current dir
repos=(`echo */ | sed 's/\///g' | tr " " "\n"`)
main_url="git@github.com:Fabiokleis/rust_gang.git"

# add new main origin and pushs to github
for i in "${repos[@]}"; do
    cd $i
    git remote rm origin
    git remote add origin $main_url;
    git branch -m `git branch | sed 's/* //g'` $i
    git push origin $i
    cd ..
done
